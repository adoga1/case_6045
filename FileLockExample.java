import java.io.File;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;

public class FileLockExample {

    public static void main(String[] args) {
        String filePath = "example.txt";

        // Create a thread that tries to lock the file
        Thread thread1 = new Thread(() -> lockFile(filePath));
        // Create another thread that tries to lock the same file
        Thread thread2 = new Thread(() -> lockFile(filePath));

        // Start both threads
        thread1.start();
        thread2.start();

        try {
            // Wait for both threads to finish
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void lockFile(String filePath) {
        try {
            // Open the file in read-write mode
            RandomAccessFile file = new RandomAccessFile(filePath, "rw");
            FileChannel fileChannel = file.getChannel();

            // Try to acquire an exclusive lock on the file
            FileLock lock = fileChannel.lock();

            if (lock != null) {
                System.out.println(Thread.currentThread().getName() + " acquired lock on " + filePath);
                // Simulate some work with the file
                Thread.sleep(1000);

                // Release the lock
                lock.release();
                System.out.println(Thread.currentThread().getName() + " released lock on " + filePath);
            }

            fileChannel.close();
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
